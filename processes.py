import sys
import config


def ampli(sound, factor: float):

    nsamples = len(sound)

    if factor > 0:
        for i in range(nsamples):
            if sound[i]*factor > config.max_amp:
                sound[i] = config.max_amp
            elif sound[i]*factor < config.min_amp:
                sound[i] = config.min_amp
            else:
                sound[i] = sound[i]*factor

        return sound

    else:
        sys.exit("Invalid ampli factor")


def reduce(sound, factor: int):

    if factor > 0:
        del sound[::factor]

        return sound

    else:
        sys.exit("Invalid reduce factor")


def extend(sound, factor: int):

    nsamples = len(sound)

    n = 0

    if factor > 0:
        for i in range(factor, nsamples, factor):
            if i > factor:
                n += 1
                i = i+n
                sound.insert(i, (sound[i - 1] + sound[i]) // 2)
            else:
                sound.insert(i, (sound[i-1] + sound[i]) // 2)

        return sound

    else:
        sys.exit("Invalid extend factor")
